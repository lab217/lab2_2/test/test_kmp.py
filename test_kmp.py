from src.app import util

solution = 'abcabeabcabcabdrabcabdtuyeabcabdwrabababalidsahglah;babababt'
elem = 'ea'
print(util.listes)


def test_correctness_search():
    result = util.kmp(solution, elem)
    assert result == solution.index(elem)


def test_correctness_input_data():
    result = elem
    result2 = solution
    assert result == str(result)
    assert result2 == str(result2)


# def test_correctness_search():
#     kmp = KMP()
#     assert (kmp.search(t1, p1) == [0, 1, 2, 3, 4, 5, 6])
#
#     p2 = "abc"
#     t2 = "abdabeabfabc"
#
#     assert (kmp.search(t2, p2) == [9])
#
#     p3 = "aab"
#     t3 = "aaabaacbaab"
#
#     assert (kmp.search(t3, p3) == [1, 8])